export type WishlistItem = {
    id: string,
    name: string,
    url?: string,
    description?: string
    image?: string,
}

export type WishlistCssClasses = {
    hideElement?: 'd-none'
}

export type WishlistSelectors = {
    wishlist?: string,
    wishlistContainer?: string,
    showWishlist?: string,
    addToWishlist?: string,
    deleteFromWishlist?: string,
    clearWishlist?: string,
    wishlistActions?: string,
    wishlistCount?: string
}

export type WishlistTranslations = {
    textItemAdded?: string,
    wishlistButtonText?: string,
    noItemsOnWishlist?: string
}

export type WishlistOptions = {
    storage?: Storage,
    selectors?: WishlistSelectors,
    translations?: WishlistTranslations
    debug?: Boolean,
    classes?: WishlistCssClasses
}

const defaultOptions: WishlistOptions = {
    storage: sessionStorage,
    debug: false,
    selectors: {
        wishlist: '.wishlist',
        wishlistContainer: '..wishlist-container',
        showWishlist: '.show-wishlist',
        addToWishlist: '.add-to-wishlist',
        deleteFromWishlist: '.delete-from-wishlist',
        clearWishlist: '.clear-wishlist',
        wishlistActions: '.wishlist-actions',
        wishlistCount: '.wishlist-count'
    },
    classes: {
        hideElement: 'd-none'
    },
    translations: {
        textItemAdded: 'zum Merkzettel hinzugefügt ✓',
        wishlistButtonText: 'Auf den Merkzettel',
        noItemsOnWishlist: 'Keine Produkte im Merkzettel'
    }
}

export class Wishlist {

    private _storage?: Storage;
    private _selector?: WishlistSelectors;
    private _translations?: WishlistTranslations;
    private _items: Array<WishlistItem>;
    private _classes?: WishlistCssClasses;
    private readonly _debug?: Boolean;

    private _showWishlist ?: HTMLElement|null;
    private _deleteFromWishlist ?: HTMLElement|null;
    private _addToWishlist ?: NodeListOf<HTMLElement>;
    private _clearWishlist ?: HTMLElement|null;
    private _wishlist ?: HTMLElement|null;
    private _wishlistContainer ?: HTMLElement|null;

    constructor(options: WishlistOptions = defaultOptions) {
        this._storage = options.storage ?? sessionStorage;
        this._selector = options.selectors ?? defaultOptions.selectors;
        this._translations = options.translations ?? defaultOptions.translations;
        this._classes = options.classes ?? defaultOptions.classes;
        this._debug = options.debug ?? defaultOptions.debug;
        this._items = this.items;

        this._loadHtmlElements();
        this._setAddToWishlistButtonTexts();
    }

    private _loadHtmlElements() {
        this._showWishlist = document.querySelector(this._selector?.showWishlist ?? '');
        this._deleteFromWishlist = document.querySelector(this._selector?.deleteFromWishlist ?? '');
        this._addToWishlist = document.querySelectorAll(this._selector?.addToWishlist ?? '');
        this._clearWishlist = document.querySelector(this._selector?.clearWishlist ?? '');
        this._wishlist = document.querySelector(this._selector?.wishlist ?? '');
        this._wishlistContainer = document.querySelector(this._selector?.wishlistContainer ?? '');

        if (this._wishlist) {
            this._wishlist.addEventListener('click', this._handleWishlistItemClick.bind(this));
        }
    }

    private _handleWishlistItemClick(e: Event) {
        if (e.target) {
            const element: HTMLElement = e.target as HTMLElement;
            const id = element.getAttribute('data-wishlist-id');

            const wishlistItem = this.items.find((item: WishlistItem) => item.id === id);

            if (wishlistItem) {
                this._removeItem(wishlistItem);
            }
        }
    }

    private _setAddToWishlistButtonTexts() {
        this.items.map((item: WishlistItem) => {
            const htmlElement = this._findElementById(item.id)

            if (htmlElement && this._translations) {
                this._translations.wishlistButtonText = htmlElement.innerHTML;
                htmlElement.innerHTML = this._translations?.textItemAdded ?? '';
            }
        })
    }

    private _wishlistItemExists(id: string) {
        return this.items.find((item: WishlistItem) => item.id === id) !== undefined;
    }

    public _findElementById(id: string) {
        this._log(`findItemById: ${id}`);

        if (this._selector?.addToWishlist) {
            const selector = `${this._selector?.addToWishlist}[data-wishlist-id="${id}"]`

            return document.querySelector(selector);
        }

        return null;
    }

    public listItems() {
        if (this._wishlistContainer) {
            this._wishlistContainer.innerHTML = '';

            this.items.forEach((item: WishlistItem) => {
                const unorderedListElement = document.createElement('li');
                const unorderedListElementLink = document.createElement('a');

                unorderedListElementLink.classList.add(this._selector?.deleteFromWishlist ?? '');
                unorderedListElementLink.setAttribute('data-wishlist-id', item.id);
                unorderedListElementLink.setAttribute('data-name', item.name);
                unorderedListElementLink.innerText = '(X)';

                unorderedListElement.appendChild(unorderedListElementLink);

                this._wishlistContainer?.appendChild(unorderedListElement);
            });

            const wishlistActions = document.querySelector(this._selector?.wishlistActions ?? '');

            if (wishlistActions) {
                wishlistActions.classList.remove(this._classes?.hideElement ?? '');
            }
        }
    }

    public itemCount() {
        const count = this.items.length;
        const wishlistCount: HTMLElement|null = document.querySelector(this._selector?.wishlistCount ?? '');

        if (!wishlistCount) {
            return;
        }

        wishlistCount.innerText = count.toString();

        if (count < 1) {
            wishlistCount.classList.add(this._classes?.hideElement ?? '');

            if (this._wishlistContainer) {
                this._wishlistContainer.innerHTML = this._translations?.noItemsOnWishlist ?? 'No products on wishlist';
            }
        } else {
            wishlistCount.classList.remove(this._classes?.hideElement ?? '');
        }
    }

    public get items(): Array<WishlistItem> {
        this._log('getItems', this._items);
        return JSON.parse(this._storage?.getItem('wishlist') ?? '[]');
    }

    public set items(items: Array<WishlistItem>) {
        this._log('setItems');
        this._items = items;

        this._storage?.setItem('wishlist', JSON.stringify(items));
    }

    public addItem(item: WishlistItem) {
        this._log('addItem');
        this.items.push(item);
    }

    private _removeItem(item: WishlistItem) {
        this._log('removeItem');
        this.items.splice(this.items.indexOf(item), 1);

        const htmlElement = this._findElementById(item.id);

        if (htmlElement) {
            htmlElement.innerHTML = this._translations?.wishlistButtonText ?? '';
        }
    }

    private _clear() {
        this._log('clear');
        this.items = [];
    }

    private _log(...messages: Array<any>) {
        if (this._debug) {
            console.log(messages);
        }
    }

    public set translations(value: WishlistTranslations) {
        this._translations = value;
    }
}
